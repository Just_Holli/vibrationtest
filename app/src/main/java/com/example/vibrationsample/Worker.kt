package com.example.vibrationsample

import android.content.Context
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.os.VibratorManager
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters

class TestWorker(context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {
    private val vibrator: Vibrator =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            (context.getSystemService(Context.VIBRATOR_MANAGER_SERVICE) as VibratorManager).defaultVibrator
        } else {
            context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        }

    fun vibrate() {
        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> {
                vibrator.vibrate(
                    VibrationEffect.createWaveform(
                        longArrayOf(0, 500, 1000, 500, 1000),
                        1
                    )
                )
            }
            else -> vibrator.vibrate(
                VibrationEffect.createWaveform(
                    longArrayOf(0, 500, 1000, 500, 1000),
                    1
                )
            )
        }
    }

    override fun doWork(): Result {
        vibrate()
        Log.e("Worker","Scuccess")
        return Result.success()
    }
}